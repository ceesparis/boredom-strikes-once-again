import './ActivityOutput.css'

const ActivityOutput = ({ currentActivity }) => {
    return (
        <>
            {currentActivity && 
                <div>
                    <h3>Why don't you do this?</h3>
                    <div className="ActivityCard">
                        <h4>{currentActivity.activity}</h4>
                        <ul className="ActivityInfo">
                            <li>Activity type: {currentActivity.type}</li>
                            <li>Participants: {currentActivity.participants}</li>
                        </ul>
                    </div>
                </div>}
        </>
    )
}

export default ActivityOutput


// "activity": "Learn Express.js",
// 	"accessibility": 0.25,
// 	"type": "education",
// 	"participants": 1,
// 	"price": 0.1,
// 	"link": "https://expressjs.com/",
// 	"key": "3943506"