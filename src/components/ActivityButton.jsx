import './ActivityButton.css';

const ActivityButton = ({ handleSubmit }) => {

    return (
        <>
            <button type="submit" onClick={() => handleSubmit()} className="ActivityButton">click here if bored</button>
        </>
    )

}

export default ActivityButton