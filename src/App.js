import './App.css';
import Activity from './views/Activity';

function App() {
  return (
    <div className="App">
        <Activity/>
    </div>
  );
}

export default App;
