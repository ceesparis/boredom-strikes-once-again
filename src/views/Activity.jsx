import ActivityButton from "../components/ActivityButton"
import ActivityOutput from "../components/ActivityOutput"
import { useState } from 'react'

import './Activity.css';

const url = 'http://www.boredapi.com/api/activity/'

const Activity = () => {

    const [ activity, setActivity ] = useState(null)
    const [ loading, setLoading ] = useState(false)

    const fetchActivity = async () => {
        try {
            const response = await fetch(url)
            if (!response.ok){
                throw new Error('could not fetch activity')
            }
            const result = await response.json()
            console.log(result)
            return [ result, null ];
        } catch (error) {
            console.log(error.message)
            return [ null, error ];
        }
    }

    const handleSubmit = async () => {
        setLoading(true)
        const [ newActivity, error ] = await fetchActivity()
        if (newActivity !== null) {
            setActivity(newActivity)
        }
        setLoading(false)
    }

    return (
        <>
            <div className="Header">
                <img src="https://cdn-icons.flaticon.com/png/512/3121/premium/3121559.png?token=exp=1650449877~hmac=0c895663b5b2eac56881f53c7faf5c08" alt="bored  guy"></img>
                <h1>End Your Boredom!</h1>
            </div>
            <div className="Activity">
                <ActivityButton handleSubmit={ handleSubmit }/>
                <div className="Loading">
                    { loading ? <p>Loading...</p> : <p></p> }
                </div>
                <ActivityOutput currentActivity= { activity } />
            </div>
        </>
    )
}

export default Activity